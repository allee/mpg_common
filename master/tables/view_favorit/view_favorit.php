<?php

class tables_view_favorit { 

  // check login & editieren nicht erlaubt
  function getPermissions($record) {
    $auth =& Dataface_AuthenticationTool::getInstance();
    $user =& $auth->getLoggedInUser();
    if ( !isset($user) ) return Dataface_PermissionsTool::NO_ACCESS();
    return Dataface_PermissionsTool::getRolePermissions('READ ONLY');
  }

  // table read only 
  function getRoles(&$record) {
    return 'READ ONLY';
  }

  function reiter__renderCell( &$record ) {
    $table = $record->strval('reiter');
    return '<a href="'.DATAFACE_SITE_HREF.'?-table='.$table.'">'.$table.'</a>';
  }

  function block__after_result_list_content() {
    $url  = DATAFACE_SITE_HREF."?-table=list_reiter&-action=list";
    echo 'Zum hinzuf&uuml;gen setze Checkbox Favorit in der gew&uuml;nschten Tabelle <a href='.$url.'>list_reiter</a>';
  }

  function autoID__renderCell(&$record) {
    $table  = 'list_reiter';
    $action = 'edit';
    $field  = 'autoID';
    $tabID  = $record->val($field);
    $name   = $tabID;
    $url    = DATAFACE_SITE_HREF."?-table=${table}&-action=${action}&${field}=${tabID}";
    return '<div style="text-align:left;"><a href="'.$url.'">'.$name.'</a></div>';
  }

}

?>
