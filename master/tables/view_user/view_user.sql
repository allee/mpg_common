-- mpi-dcts own version aus user-db
CREATE OR REPLACE VIEW view_user AS
SELECT
 localID, last_name, first_name FROM mpidb_user.mpi_user
ORDER BY
 localID
;

-- mpg-version aus user-db
CREATE OR REPLACE VIEW view_user AS
SELECT
 localID, last_name, first_name FROM mpidb_mpg_user.mpi_user
WHERE
 active = 1
ORDER BY
 localID
;

-- mpg-version ohne externe DB (licman)
CREATE OR REPLACE VIEW mpidb_mpg_licman.view_user AS
SELECT 
 'local' AS localID, 'version' AS last_name, 'mpg' AS first_name
ORDER BY 
 localID
;

-- mpg-version mit externe DB (licmann)
CREATE OR REPLACE VIEW mpidb_mpg_licman.view_user AS
SELECT 
 localID, last_name, first_name FROM mpidb_mpg_user.mpi_user
WHERE
 active = 1
ORDER BY 
 localID
;

-- mpg-version ohne externe DB (inv)
CREATE OR REPLACE VIEW mpidb_mpg_inv.view_user AS
SELECT 
 'local' AS localID, 'version' AS last_name, 'mpg' AS first_name
ORDER BY 
 localID
;

-- mpg-version mit externe DB (inv)
CREATE OR REPLACE VIEW mpidb_mpg_inv.view_user AS
SELECT 
 localID, last_name, first_name FROM mpidb_mpg_user.mpi_user
WHERE
 active = 1
ORDER BY 
 localID
;
