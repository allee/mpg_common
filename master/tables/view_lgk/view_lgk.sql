CREATE OR REPLACE VIEW view_lgk AS
SELECT
 autoID,
 lgk,
 beschreibung,
 priority
FROM
 mpidb_asi_inv.list_lgk
