<?php

class tables_list_reiter { 

/*
  // geht leider wegen xataface parser nicht, deshalb per view
  function __sql__() {
    $app = Dataface_Application::getInstance();
    $db  = $app->_conf['_database']['name'];
    return "SELECT lst.*, IF(shw.reiter IS NULL, 0, 1) AS exist FROM list_reiter AS lst LEFT JOIN ( SELECT CONVERT(table_name USING utf8) COLLATE utf8_unicode_ci AS reiter, table_type FROM information_schema.tables WHERE table_schema = '$db' AND ( table_type = 'base table' OR table_type = 'view' ))AS shw ON lst.reiter = shw.reiter";
  }
*/

  // loeschen nur fuer admin oder manager erlaubt
  function getPermissions($record) {
    $auth =& Dataface_AuthenticationTool::getInstance();
    $user =& $auth->getLoggedInUser();
    if ( !isset($user) ) return Dataface_PermissionsTool::NO_ACCESS();
    $role = $user->val('role'); // get Role from mpi_users
    if ( $role == 'MANAGER' or $role == 'ADMIN') return;
    return Dataface_PermissionsTool::getRolePermissions('EDIT');
  }

  // zeige alle noch nicht erfassten tabellen als auswahl
  function valuelist__tables() {
    $app = Dataface_Application::getInstance();
    $db  = $app->_conf['_database']['name'];
    static $user = -1;
    if ( !is_array($tables) ) {
      $tables = array();
      $res = mysql_query("SHOW FULL TABLES FROM $db WHERE NOT EXISTS ( SELECT * FROM list_reiter AS lst WHERE lst.reiter = Tables_in_$db COLLATE utf8_unicode_ci )", df_db());
      if ( !$res ) throw new Exception(mysql_error(df_db()));
      while ($row = mysql_fetch_row($res)) $tables[$row[0]] = $row[1].' : '.$row[0];
    }
    return $tables;
  }

  function reiter__renderCell( &$record ) {
    $table = $record->strval('reiter');
    if ( $record->strval('exist') == '0' ) return '<font color="red">'.$table.'</font>';
    return '<a href="'.DATAFACE_SITE_HREF.'?-table='.$table.'">'.$table.'</a>';
  }

  function beforeSave(&$record) {
    if ($record->strval('bedeutung') == NULL) {
      $record->setValue('bedeutung', $record->strval('reiter'));
    }
  }

}
?> 
