--
-- Tabellenstruktur für Tabelle `list_reiter`
--

CREATE TABLE IF NOT EXISTS `list_reiter` (
  `autoID` smallint(6) unsigned zerofill NOT NULL AUTO_INCREMENT,
  `reiter` varchar(30) COLLATE utf8_unicode_ci NOT NULL,
  `kategorie` varchar(30) COLLATE utf8_unicode_ci NOT NULL,
  `favorit` tinyint(1) NOT NULL DEFAULT '0',
  `bedeutung` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`autoID`),
  UNIQUE KEY `reiter` (`reiter`),
  KEY `kategorie` (`kategorie`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci COMMENT='Reiterlinks fuer Xataface' AUTO_INCREMENT=416 ;

--
-- RELATIONEN DER TABELLE `list_reiter`:
--   `kategorie`
--       `list_katReiter` -> `kategorie`
--

--
-- Daten für Tabelle `list_reiter`
--

INSERT INTO `list_reiter` (`autoID`, `reiter`, `kategorie`, `favorit`, `bedeutung`) VALUES
(000001, 'list_email', 'Liste', 0, 'Emailadresse fuer Benachrichtung Ablauf Lizenz oder Support'),
(000002, 'list_hersteller', 'Liste', 0, 'Liste Hersteller fuer Software und Vereinbarungen'),
(000003, 'list_kategorie', 'Liste', 0, 'Liste Software-Kategorien'),
(000004, 'list_katReiter', 'Liste', 0, 'Zugehoerigkeit DB-Tabellen'),
(000005, 'list_lagerort', 'Liste', 0, 'Ablageort Schlussel'),
(000006, 'list_lieferant', 'Liste', 0, 'Liste Lieferanten fuer Lizenzen und Vereinbarungen'),
(000007, 'list_logintyp', 'Liste', 0, 'Zuordnung Links zu einer Kategorie'),
(000018, 'list_nutzer', 'Liste', 0, 'Liste Nutzer in Lizenzen und Installationen'),
(000019, 'list_reiter', 'Liste', 0, 'Sammelcontainer fuer Tabbutton "mehr .."'),
(000020, 'list_vorgang', 'Liste', 0, 'Zuordnung Dateiablage zu einer Kategorie'),
(000021, 'mpi_ablage', 'Ablage', 1, 'Dateiablagen fuer Software und Lizenzen'),
(000022, 'mpi_links', 'Ablage', 0, 'Linksammlung zu diversen Webseiten oder Logins'),
(000023, 'mpi_notiz', 'Ablage', 0, 'Liste Lizenznotizen'),
(000024, 'mpi_users', 'Authorisierung', 1, 'Authorisierung und Berechtigung Benutzer'),
(000025, 'view_user', 'View', 0, 'Auswahl Benutzer aus DB user'),


--
-- Constraints der Tabelle `list_reiter`
--
ALTER TABLE `list_reiter`
  ADD CONSTRAINT `list_reiter_kateg` FOREIGN KEY (`kategorie`) REFERENCES `list_katReiter` (`kategorie`) ON UPDATE CASCADE;

--
-- Erweiterung vorhandener Tabellen
--
 ALTER TABLE list_reiter MODIFY COLUMN bedeutung varchar (100) AFTER kategorie;
 ALTER TABLE list_reiter ADD favorit TINYINT(1) NOT NULL DEFAULT '0' AFTER kategorie;
 ALTER TABLE list_reiter ADD history TINYINT(1) NOT NULL DEFAULT '0' AFTER favorit;
 ALTER TABLE list_reiter CHANGE autoID tabID SMALLINT(6) UNSIGNED ZEROFILL NOT NULL AUTO_INCREMENT;
 DROP TABLE IF EXISTS list_reiter__history;
 CREATE OR REPLACE VIEW view_favorit AS SELECT * FROM list_reiter WHERE favorit = '1';

