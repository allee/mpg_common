CREATE OR REPLACE VIEW view_gefahr AS
SELECT
 autoID,
 kategorie,
 hsatz,
 merkmal,
 anweisung,
 lgk
FROM
 mpidb_asi_inv.list_gefahr
