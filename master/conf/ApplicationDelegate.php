<?php
/**
 * A delegate class for the entire application to handle custom handling of 
 * some functions such as permissions and preferences.
 */
class conf_ApplicationDelegate
{
  /**
   * Returns permissions array.  This method is called every time an action is 
   * performed to make sure that the user has permission to perform the action.
   * @param record A Dataface_Record object (may be null) against which we check
   *               permissions.
   * @see Dataface_PermissionsTool
   * @see Dataface_AuthenticationTool
   */

  function getPermissions(&$record)
  {
    $auth =& Dataface_AuthenticationTool::getInstance();
    $user =& $auth->getLoggedInUser();
    // if the user is null then nobody is logged in... no access.
    if ( !isset($user) ) return Dataface_PermissionsTool::NO_ACCESS();
    // This will force a login prompt.
    $role = $user->val('role');
    return Dataface_PermissionsTool::getRolePermissions($role);
    // Returns all of the permissions for the user's current role.
  }


  function block__tables_menu_tail()
  {
    $user = Dataface_AuthenticationTool::getInstance()->getLoggedInUser();
    // if the user is null then nobody is logged in... no access.
    if ( !isset($user) ) return Dataface_PermissionsTool::NO_ACCESS();
    // This will force a login prompt.
    $role = $user->val('role'); // get Role from mpi_users
    if ( $role == 'MANAGER' or $role == 'ADMIN')
    {
      $app =& Dataface_Application::getInstance();
      $query =& $app->getQuery();
      $url = basename(DATAFACE_SITE_URL);
      $cnt = count($app->_tables); 
      if ( count($app->_tables) <= 13 ) 
      // entsprechend dataface/Dataface/Application.php
      {
        echo '   <li><a href="/'.$url.'/index.php?-table=mpi_users";
                          accesskey="accesskeys-navigation";
                          class="table-selection-tab";
                          title="Users";
                          id="TableLink_mpi_users">
                                  Benutzer
                 </a></li>';
      } else {
        echo '   <div class="portletContent">
                  <a href="/'.$url.'/index.php?-table=mpi_users"
                           accesskey="accesskeys-navigation"
                           class="navItem "
                           title="Benutzer"
                           id="TableLink_mpi_users">
                   <img src="'.DATAFACE_URL.'/images/folder_icon.gif" alt="" class="navIconRoot" title="Benutzer" />
                   <span class="navItemText"> Benutzer </span>
                  </a>
                 </div>';
      }
    }
  }
}
?>
