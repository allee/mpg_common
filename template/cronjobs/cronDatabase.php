<?php

// Cronjob Skript fuer Aktionen ausserhalb der DB
// z.B. Mailbenachrichtigung, Loeschen verwaister Ablagen etc.
// Beispiele folgend
// schachi 2016-02-15

  // stelle sicher das dieses Skript in einem Subdir liegt, normalerweise im Ordner cronjobs, sonst gibt es kausale Problem :-(
  chdir(__DIR__);
  chdir('../');
  if (!is_readable('conf.ini') ) die ('Error loading config file from here '.getcwd()."\n");

  $conf = array();
  // $conf = array_merge(parse_ini_file('conf.ini', true), $conf);
  $conf = parse_ini_file('conf.ini', true);
  if ( !isset( $conf['_database'] ) ) {
    die ('Error loading config file. No database specified.'); 
  }
  $dbinfo =& $conf['_database'];
  //print_r ($dbinfo['host'].' '.$dbinfo['user'].' '.$dbinfo['password']);
  if ( !is_array( $dbinfo ) || !isset($dbinfo['host']) || !isset( $dbinfo['user'] ) || !isset( $dbinfo['password'] ) || !isset( $dbinfo['name'] ) ) {
    die ('Error loading config file.  The database information was not entered correctly.');
  }
  if ( @$dbinfo['persistent'] ) {
    $db = mysql_pconnect( $dbinfo['host'], $dbinfo['user'], $dbinfo['password'] );
  } else {
    $db = mysql_connect( $dbinfo['host'], $dbinfo['user'], $dbinfo['password'] );
  }
  if ( !$db ) {
    die ('Error connecting to the database' . mysql_error());
  }
  $mysqlVersion = mysql_get_server_info($db);
  mysql_select_db( $dbinfo['name'] ) or die("Could not select DB: ".mysql_error($db));



  // lege alle DS an, welche versioniert sind und noch nicht existieren
  $sql = "INSERT IGNORE INTO show_version (verID) SELECT DISTINCT verID FROM con_verLic WHERE 1";
  mysql_query($sql) or die(mysql_error($db));
  // hole von jeder software eine version
  $sql = "SELECT ver.verID FROM mpi_version AS ver INNER JOIN con_verLic AS con ON con.verID = ver.verID GROUP BY softID";
  $query = mysql_query($sql) or die(mysql_error());
  $count = mysql_num_rows($query);
  if ( $count >= 1 ) {
    while($row = mysql_fetch_assoc($query)) {
      $softID = $row['verID'];
      $sql = "CALL proc_version($softID)";
      //echo $sql."\n";
      mysql_query($sql) or die(mysql_error($db));
    }
  }



  // loesche alle <table>__history, welche nicht erwuenscht sind
  // Xataface hat nur einen globalen Schalter ON/OFF fuer history, aber wer braucht denn alle histories?
  $sql = "SELECT reiter FROM view_reiter WHERE reiter IN (SELECT CONCAT(lst.reiter, '__history') AS table_his FROM list_reiter AS lst LEFT JOIN view_reiter AS vReit ON lst.reiter = vReit.reiter WHERE lst.history = '0' AND vReit.table_type = 'BASE TABLE' AND lst.reiter NOT LIKE '%__history') AND table_type = 'BASE TABLE';";
  $query = mysql_query($sql) or die(mysql_error());
  $count = mysql_num_rows($query);
  if ( $count >= 1 ) {
    while($row = mysql_fetch_assoc($query)) {
      $table = $row['reiter'];
      $sql = "DROP TABLE IF EXISTS $table;"; 
      //echo $sql."\n";
      mysql_query($sql) or die(mysql_error($db));
    }
  }



  // loesche alle Ablagen, welche keine Verbindung mehr haben
  $sql = "DELETE FROM mpi_ablage WHERE softID IS NULL AND vertragID IS NULL AND lizenzID IS NULL AND srvID IS NULL;";
  mysql_query($sql) or die(mysql_error());



  // Script fuer automatisches senden von emails, wenn Ablaufdatum naht.
  // schachi 2015-04-08
  if (!isset( $conf['_own']['notify'] )) $delay = 30; else $delay = $conf['_own']['notify']; 
  if (!isset( $conf['_own']['dn'] )) $dn = 'mpi-magdeburg.mpg.de'; else $dn = $conf['_own']['dn']; 
  $mail = 'itapp@'.$dn;

  function sendMail($pre, $betr, $delay, $base, $mail) {
    switch ($betr) {
      case 'Support':
      case 'Lizenz':
        $table = 'mpi_lizenz';
        $field = 'lizenzID';
        break;
      case 'Vertrag':
        $table = 'mpi_vertrag';
        $field = 'vertragID';
        $ende  = 'heute!';
        break;
    default:
      die ('Error choice subject.');
    }
    $sql = <<<EOT
  SELECT software, lizenzID, vertragID, email, expireDate, anz, body
  FROM view_sendMail
  WHERE ( expireDate = ADDDATE( CURDATE(), $delay )) AND ( body = '$betr' );
EOT;
    //print_r ($sql."\n");
    $query = mysql_query($sql) or die(mysql_error());
    $count = mysql_num_rows($query);
    if ( $count >= 1 ) {
      $host = gethostname();
      $path = basename(__DIR__);
      while($row = mysql_fetch_array($query)) {
        $tabID = $row[$field];
        $soft  = $row['software'];
        $mail  = $row['email'];
        if ( $delay > 0 ) $ende = 'in '.$delay.' Tagen.'; else $ende = 'heute!';
        $body  = $pre.$betr.' der Software '.$soft.' endet '.$ende;
        $text  = "Hier der direkte Link zum Datensatz:\n";
        $link  = 'http://'.$host.'/'.$path.'/index.php?-table='.$table.'&-action=browse&'.$field.'='.$tabID;
        $head  = "From: Database ".$base." <".$mail.">\n";
        $head .= "Content-Type: text/plain; charset=utf-8\n";
        $head .= "MIME-Version: 1.0\n";
        //print_r ( $mail.' - '.$body.' - '.$text.$link.' - '.$head );
        mail( $mail, $body, $text.$link, $head );
      }
    }
  }

  $base = $dbinfo['name'];
  $betr = array('Support','Lizenz','Vertrag');
  foreach ($betr as $body) {
    sendMail( '[INFO] ',    $body, $delay, $base, $mail );
    sendMail( '[WARNUNG] ', $body, '7',    $base, $mail );
    sendMail( '[WICHTIG] ', $body, '0',    $base, $mail );
  }


  mysql_close($db);

?>

