<?php
/**
 * File: index.php
 * Description:
 * -------------
 *
 * This is an entry file for this Dataface Application.  To use your application
 * simply point your web browser to this file.
 */
  // Limit veraendern
  if ( !isset($_REQUEST['-limit']) ) {
     $_REQUEST['-limit'] = $_GET['-limit'] = '20';
  }
  // Sortierreihenfolge in Listenansicht aendern
  //if ( !isset($_REQUEST['-sort']) and @$_REQUEST['-table'] == 'mpi_lieferant' ) {
  //    $_REQUEST['-sort'] = $_GET['-sort'] = 'firma';
  //}

 // disbale table views in db - sonst kommt immer Fehlermeldung in apache
define('XATAFACE_DISABLE_PROXY_VIEWS',true);

require_once '../xataface/dataface-public-api.php';
df_init(__FILE__, "../xataface")->display();
