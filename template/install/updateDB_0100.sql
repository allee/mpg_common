-- run: mysql -u root -p mpidb_mech_inv < updateDB_0500.sql
-- UPDATES immer von der niefrigsten bis zur hoechsten version ausfuehren
-- damit IF benutzt werden kann, wird eine prozedur erzeugt und am ende ausgefuehrt
-- 
-- views, funcs, procs nach moeglichkeit nur einmal in der max version ausfuehren

-- USE mpidb_mpg_licman;
DROP PROCEDURE IF EXISTS proc_update;
DELIMITER $$
CREATE PROCEDURE proc_update()
proc_label: BEGIN

-- initial value
IF ( SELECT MAX(version) FROM dataface__version ) = '0' THEN
 TRUNCATE dataface__version;
 INSERT INTO dataface__version (version) VALUES ('0100');
END IF;

-- mindest version vorhanden
IF ( SELECT MAX(version) FROM dataface__version ) < '0100' THEN
 LEAVE proc_label;
END IF;


IF ( SELECT MAX(version) FROM dataface__version ) < '0100' THEN

-- CHANGES V0.5.00 :
-- ****************
-- fs::rsync - mech_inv initial

 TRUNCATE dataface__version;
 INSERT INTO dataface__version (version) VALUES ('0100');

END IF;




END;
$$
DELIMITER ;

-- execute updates
CALL proc_update();
