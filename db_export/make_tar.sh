#!/bin/bash

# create tar's from all projects
# schachi 2016-02-13

cd /var/www

echo "create tar from common folders"
tar --exclude-vcs -czf db_export/mpidb_mpg_common.tar.gz xataface xataface-2.1.2/ master/ template/ favicon.ico index.html info.php

for project in user inv licman; do
  echo "Clear files in mpg_${project}/templates_c"
  rm -R mpg_${project}/templates_c/*
  echo "create tar from plain project mpg_${project}"
  tar --exclude-vcs -czf db_export/mpidb_mpg_${project}.tar.gz mpg_${project}/
  echo "create tar from project mpg_${project} with common files"
  tar --exclude-vcs -czf db_export/mpidb_mpg_${project}_full.tar.gz xataface xataface-2.1.2/ master/ template/ favicon.ico index.html info.php LICENSE.txt mpg_${project}/
done
